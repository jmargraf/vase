import numpy as np
from ase.io import read
from ase import neighborlist
from ase.data import covalent_radii,vdw_radii

import pyvista as pv

from ase.data.colors import jmol_colors

pv.global_theme.title = 'vase'

BondRadius = 0.1
BondRes    = 25
AtomRes    = 75
ScaleAtoms = 0.3

CovRadCut  = 1.4


def get_bonds(mol):
    cutOff = neighborlist.natural_cutoffs(mol,mult=CovRadCut)
    iBond, jBond, rBond = neighborlist.neighbor_list('ijD',mol,cutOff, self_interaction=False)
    return iBond, jBond, rBond

def show_molecule(mol):
    iBond, jBond, rBond = get_bonds(mol)
    p = pv.Plotter(window_size=[768,768])
    p.set_background('white')
    Zs = mol.get_atomic_numbers()
    com = mol.get_center_of_mass()
    pos = mol.get_positions()-com
    formula = mol.get_chemical_formula()
    for i,Z in enumerate(Zs):
        atom = pv.Sphere(center=pos[i],
                         radius=vdw_radii[Z]*ScaleAtoms,
                         theta_resolution=AtomRes,
                         phi_resolution=AtomRes)
        p.add_mesh(atom,
                   color=jmol_colors[Z],
                   roughness=1.0,
                   render=False)

    for i,r in enumerate(rBond):
        bond = pv.Cylinder(center=pos[iBond[i]]+0.5*r,
                           direction=r,
                           height=np.linalg.norm(r),
                           radius=BondRadius,
                           resolution=BondRes,
                           render=False)
        p.add_mesh(bond,color=(0.1,0.1,0.1))
    p.view_isometric(negative=True)
    act = p.add_text(formula,color='black')
    p.show()

def show_trajectory(mol_list):
    nFrames = len(mol_list)
    print('Plotting ',nFrames,' images')
    iBond_list = []
    jBond_list = []
    rBond_list = []
    Zs_list    = []
    pos_list   = []
    formula_list = []
    for mol in mol_list:
        iBond, jBond, rBond = get_bonds(mol)
        Zs = mol.get_atomic_numbers()
        com = mol.get_center_of_mass()
        pos = mol.get_positions() - com
        formula_list.append(mol.get_chemical_formula())
        iBond_list.append(iBond)
        jBond_list.append(jBond)
        rBond_list.append(rBond)
        Zs_list.append(Zs)
        pos_list.append(pos)

    p = pv.Plotter(window_size=[768,768])
    p.set_background('white')
    p.enable_parallel_projection()
    actors = []
    def update_view(iFrame):
        for actor in actors:
            p.remove_actor(actor,render=False)
        iFrame = int(iFrame)
        if iFrame>nFrames-1:
            iFrame = nFrames-1
        for i,Z in enumerate(Zs_list[iFrame]):
            atom = pv.Sphere(center=pos_list[iFrame][i],
                         radius=vdw_radii[Z]*ScaleAtoms,                         
                         theta_resolution=AtomRes,
                         phi_resolution=AtomRes)
            act = p.add_mesh(atom,
                             color=jmol_colors[Z],
                             show_edges=False,
                             roughness=1.0,
                             render=False)
            actors.append(act)
        for i,r in enumerate(rBond_list[iFrame]):
            bond = pv.Cylinder(center=pos_list[iFrame][iBond_list[iFrame][i]]+0.5*r,
                               direction=r,height=np.linalg.norm(r),
                               radius=BondRadius,
                               resolution=BondRes)
            act = p.add_mesh(bond,color=(0.1,0.1,0.1),
                             render=False)
            actors.append(act)
        p.view_isometric(negative=True)
        act = p.add_text(formula_list[iFrame],color='black')
        actors.append(act)
        return

    slider = p.add_slider_widget(
    update_view,
    [0, nFrames-1],
    value=0,
    color="black",
    fmt="%0.0f",
    style='modern')
    p.show()


if __name__ == '__main__':
    mol = read('acetaldehyde.xyz@:')
    show_trajectory(mol)

    mol_list = read('closed_shell_first_row.xyz@:') 
    show_trajectory(mol_list)

