# vase - a minimalist molecular visualization tool using ASE

vase is a simple molecule viewer that uses ASE to handle molecular structure information and PyVista (and thus VTK) for rendering. 

Tested with Python 3.8

Questions can be directed to margraf@fhi.mpg.de
